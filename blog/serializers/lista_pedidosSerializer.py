from rest_framework import serializers

from blog.models.productos import Productos
from blog.models.lista_pedidos import Lista_Pedidos

from blog.serializers.productosSerializer import productosSerializer

class lista_pedidosSerializer(serializers.ModelSerializer):
    products = productosSerializer()
    class Meta:
        model = Lista_Pedidos
        fields = ['ID_Lista_Pedi','cantidad_Prod','products']

        def to_create(self, validated_data):
            productData = validated_data.pop('products')
            lista_pedidosInstance = Lista_Pedidos.objects.create(**validated_data)
            Productos.objects.create(ID_Prod=lista_pedidosInstance,**productData)
            return lista_pedidosInstance

        def to_representation(self, obj):
            lista_Pedidos = Lista_Pedidos.objects.get(ID_Lista_Pedi=obj.id)
            products = Productos.objects.get(ID_Prod=obj.id)
            return {
                'ID_Lista_Pedi': lista_Pedidos.ID_Lista_Pedi,
                'cantidad_Prod': lista_Pedidos.cantidad_Prod,
                'Productos': {
                    'ID_prodc': products.ID_prod,
                    'nombre_prod': products.nombre_prod,
                    'precio_prod': products.precio_prod
                }
            }