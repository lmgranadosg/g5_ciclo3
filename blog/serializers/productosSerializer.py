from rest_framework import serializers

from blog.models.productos import Productos

from blog.serializers.lista_pedidosSerializer import lista_pedidosSerializer

class productosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Productos
        fields = ['nombre_prod','precio_prod']