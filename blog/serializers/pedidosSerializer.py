from rest_framework import serializers

from blog.models.productos import Productos
from blog.models.lista_pedidos import Lista_Pedidos
from blog.models.pedidos import Pedidos

from blog.serializers.productosSerializer import productosSerializer
from blog.serializers.lista_pedidosSerializer import lista_pedidosSerializer

class pedidosSerializer(serializers.ModelSerializer):
    id_lista_pedido = lista_pedidosSerializer()
    class Meta:
        model = Lista_Pedidos
        fields = ['ID_Pedidos','id_lista_pedido']

        def to_create(self, validated_data):
            pedidosData = validated_data.pop('pedidos')
            pedidosInstance = Pedidos.objects.create(**validated_data)
            Lista_Pedidos.objects.create(ID_Lista_Pedi=pedidosInstance,**pedidosData)
            return pedidosInstance

        def to_representation(self, obj):
            pedidos = Pedidos.objects.get(ID_Pedidos=obj.id)
            lista_pedidos = Lista_Pedidos.objects.get(id_lista_pedido=obj.id)
            return {
                'ID_Pedidos': pedidos.ID_Pedidos,
                'ID_ListPed': lista_pedidos.ID_Lista_Pedi
            }