from rest_framework import serializers
from blog.models.lista_pedidos import Lista_Pedidos
from blog.models.pedidos import Pedidos
from blog.models.productos import Productos
from blog.models.registro_login import User

from blog.serializers.pedidosSerializer import pedidosSerializer

class registro_loginSerializer(serializers.ModelSerializer):
    id_cliente = pedidosSerializer
    class Meta:
        model = User
        fields = ['id','email','telep','passwUser','fullName','dateOfRegis','addressHouse','cityAct','yearOld','is_active','is_staff','ID_Cli']

        def create(self,validated_data):
            pedidosData = validated_data.pop('ID_Cli')
            userInstance = User.objects.create(**validated_data)
            Pedidos.objects.create(ID_Cli=userInstance, **pedidosData)
            return userInstance
        def to_representation(self,obj):
            user = User.objects.get(id=obj.id)
            pedidos = Pedidos.objects.get(ID_Cli=obj.id)
            return {
                'id': user.id,
                'email': user.email,
                'telep': user.telep,
                'fullName': user.fullName,
                'dateOfRegis': user.dateOfRegis,
                'addressHouse': user.addressHouse,
                'cityAct': user.cityAct,
                'yearOld': user.yearOld,
                'is_active': user.is_active,
                'is_staff': user.is_staff,
                'Pedidos':{
                    'id_lista_pedido': pedidos.ID_ListPed
                }
            }