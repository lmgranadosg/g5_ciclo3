from django.db import models
from django.db.models.deletion import CASCADE

from .productos import Productos
from .pedidos import Pedidos
from .Registro_Login import User

class Lista_Pedidos(models.Model):
    # ID_Lista_Pedi = models.BigAutoField(primary_key=True)
    # ID_Prod = models.BigIntegefrField(on_delete=models.CASCADE)
    # cantidad_Prod = models.BigIntegerField('Cantidad',default=1)

    ID_Lista_Pedi = models.OneToOneField(Pedidos,related_name='pedidos', primary_key=True)
    ID_Prod = models.BigIntegerField('ID producto',unique=True)
    cantidad_Prod = models.BigIntegerField('Cantidad',default=1)