from django.db import models
from django.db.models.fields.related import ForeignKey

from .lista_pedidos import Lista_Pedidos
from .Registro_Login import User
from .lista_pedidos import Lista_Pedidos

class Productos(models.Model):
    # ID_prod = models.AutoField(primary_key=True,ForeignKey=True)
    ID_prod = models.OneToOneField(Lista_Pedidos,related_name="products", primary_key=True)
    nombre_prod = models.CharField('Nombre producto',max_length=100, unique=True)
    precio_prod = models.IntegerField('Precio', default=0)
