from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def create_user(self, email, password, name, telep):
    # Crear usuario de acuerdo a la base 
        if not email and not password and not name and not telep:
            raise ValueError('Datos faltantes o inválido para el registro')

        userDef = self.model(email = email, fullName=name, telep=telep)
        userDef.set_password(password)
        userDef.save(using=self._db)

        return userDef
    def create_superuser(self, email, fullName, password, telep):
        superUser = self.create_user(email, password, fullName, telep)
        superUser.is_superuser = True
        superUser.is_staff = True
        superUser.save(using=self._db)
        return superUser

class User(AbstractBaseUser, PermissionsMixin):
    id = models.BigAutoField(primary_key=True)
    email = models.EmailField('Correo electrónico',max_length=200, unique=True)
    telep = models.CharField('Teléfono',max_length=20)
    passwUser = models.CharField('Contraseña',max_length=300)
    fullName = models.CharField('Nombre y apellido',max_length=200, blank=True)
    dateOfRegis = models.DateField('Fecha de registro',default=datetime.now)
    addressHouse = models.CharField('Dirección',max_length=300, blank=True)
    cityAct = models.CharField('Ciudad de residencia',max_length=300, blank=True)
    yearOld = models.IntegerField('Edad', default=18)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    def save(self, **kwargs):
        self.passwUser = make_password(self.passwUser, 
        'mMUj0DrIK6vgtdIYepkIxN')
        super().save(**kwargs)

    objects = UserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['fullName','telep']