from django.db import models
from .registro_login import User
from .lista_pedidos import Lista_Pedidos
from blog.models import lista_pedidos

class Pedidos(models.Model):
    ID_Pedidos = models.BigAutoField(primary_key=True)
    ID_ListPed = models.ForeignKey(lista_pedidos,related_name='id_lista_pedido',unique=True)
    ID_Cli = models.ForeignKey(User,related_name="id_cliente",on_delete=models.CASCADE)