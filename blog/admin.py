from django.contrib import admin

from .models.Registro_Login import User
from .models.pedidos import Pedidos
from .models.lista_pedidos import Lista_Pedidos
from .models.productos import Productos

admin.site.register(User)
admin.site.register(Lista_Pedidos)
admin.site.register(Productos)
admin.site.register(Pedidos)